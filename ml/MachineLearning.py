import os
import re
import pickle
from sklearn.feature_extraction.text import TfidfVectorizer
from ml.TextPreprocessing import TextPreprocessing
from sklearn.metrics.pairwise import cosine_similarity


def strip_sentence(text):
    return re.sub(r'[^\w\s]', '', text)


def save_data(input_file, output_file, fileType, permission='wb'):
    output_file = strip_sentence(output_file)
    file = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'ml\\dataset\\' + output_file + fileType + '.pk')
    pickle.dump(input_file, open(file, permission))


def mount_data(file, fileType, permission='rb'):
    file = strip_sentence(file)
    open_file = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'ml\\dataset\\' + file + fileType + '.pk')
    return pickle.load(open(open_file, permission))


def train_dataset(questions_in_category, group_name):
    # Vectorizer untuk mengonversi dataset menjadi matriks fitur TF-IDF
    vec = TfidfVectorizer(preprocessor=TextPreprocessing().text_preprocess)

    tfidf = vec.fit_transform(questions_in_category)

    # Mendapatkan isi kata vocabulary (feature name)
    vocab = vec.get_feature_names()

    vocabulary_tfidf = TfidfVectorizer().fit(vocab)

    save_data(vocabulary_tfidf, group_name, '.voc')
    save_data(tfidf, group_name, '.tfidf')


def find_similar(input_to_find, group_name, questions_in_category):
    vocabulary = mount_data(group_name, '.voc')
    tfidf = mount_data(group_name, '.tfidf')

    cos_tfidf = vocabulary.transform([TextPreprocessing().text_preprocess(input_to_find)])

    cosine_result = cosine_similarity(cos_tfidf, tfidf).flatten()

    # Get most similar jobs based on next text
    sorted_result = cosine_result.argsort()[::-1]

    response_return = []
    for i in sorted_result[:3]:
        response_return.append({
            'confidence_percentage': cosine_result[i],
            'id': questions_in_category[i].get('id'),
            'text': questions_in_category[i].get('question'),
        })

    return response_return
