import os
import re
import nltk
import textblob
from textblob import TextBlob

from nltk.corpus import stopwords
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory

from Sastrawi.Stemmer.StemmerFactory import StemmerFactory


class TextPreprocessing:
    def preprocess_regex(self, text):
        rgx_list = [
            r'[\w._%+-]+@[\w\.-]+\.[a-zA-Z]{2,4}',
            r'\b\d{8,14}\b',
            r'(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})',
            r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+',
        ]
        for rgx_match in rgx_list:
            text = re.sub(re.compile(rgx_match), '', text)

        return text

    def preprocess_translate(self, text):
        tb = TextBlob(text)
        # ini itu karena apabila text dideteksi sebagai id, sedangkan tujuan translate adalah id, akan terjadi error
        if tb.detect_language() != 'id':
            try:
                return ''.join(tb.translate(to='id'))

            except textblob.exceptions.NotTranslated:
                return ''.join(tb)

            except textblob.exceptions.TranslatorError:
                return ''.join(tb)
        else:
            return ''.join(tb)

    def preprocess_lower(self, text):
        return text.lower()

    def preprocess_punctuation(self, text):
        return re.sub(r'[^\w\s]', '', text)

    def preprocess_strip(self, text):
        return text.strip()

    def preprocess_tokenize(self, text):
        return TextBlob(text).words

    def prepare_slang(self):
        file = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'ml\\data\\slang.txt')
        df = open(file, "r", encoding="utf-8", errors='replace')
        slangS = df.readlines();
        df.close()

        slangS = [t.strip('\n').strip() for t in slangS]
        # pisahkan berdasarkan ':'
        slangS = [t.split(":") for t in slangS]
        slangS = [[k.strip(), v.strip()] for k, v in slangS]
        return {k: v for k, v in slangS}

    def preprocess_slang(self, text):
        slangS = self.prepare_slang()
        for i, t in enumerate(text):
            if t in slangS.keys():
                text[i] = slangS[t]

        return text

    def prepare_stopwords(self):
        factory = StopWordRemoverFactory()
        en_stopwords = stopwords.words('english')
        id_stopwords = factory.get_stop_words()

        # Tips: selalu rubah list stopwords ke bentuk set, karena di Python jauh lebih cepat untuk cek existence di set ketimbang list
        return {
            'id': set(id_stopwords),
            'en': set(en_stopwords)
        }

    def preprocess_stopword(self, text):
        stopwords = self.prepare_stopwords()
        texts = [t for t in text if t not in stopwords.get('en')]
        return [t for t in text if t not in stopwords.get('id')]

    """
    Fungsi preprocess_stemmer ini menggunakan fungsi yang disediakan oleh sastrawi,
    disitu juga melakukan preprocess seperti lower(), punctuation(), strip()
    """

    def preprocess_stemmer(self, text):
        factory = StemmerFactory()
        stemmer = factory.create_stemmer()
        return stemmer.stem(text)

    def text_preprocess(self, text):
        text = self.preprocess_regex(text)
        # text = self.preprocess_translate(text)
        text = self.preprocess_stemmer(text)
        text = self.preprocess_tokenize(text)
        text = self.preprocess_slang(text)
        return ' '.join(self.preprocess_stopword(text))

    def array_preprocess(self, arr):
        return [self.text_preprocess(text) for text in arr]
