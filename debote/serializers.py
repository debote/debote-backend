from django.contrib.auth.models import User, Group
from debote.models import Category, Question, DebateSession, DebateInteraction
from rest_framework import serializers


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = (
            'id',
            'question',
            'category',
        )


class CategorySerializer(serializers.ModelSerializer):
    questions = QuestionSerializer(
        many=True,
        # view_name='question-detail',
        read_only=False,
        required=False,
    )

    class Meta:
        model = Category
        fields = (
            'id',
            'category',
            'language',
            'questions',
            'created_at',
        )


class CategoryOnlySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = (
            'id',
            'category',
            'language',
            'created_at',
        )


class DebateInteractionSerializer(serializers.ModelSerializer):
    question_detail = serializers.StringRelatedField(read_only=True, source='question')

    class Meta:
        model = DebateInteraction
        fields = (
            'id',
            'debate_session',
            'response_by',
            'question',
            'question_detail',
            'answered_at',
            'confidence',
        )


class DebateSessionSerializer(serializers.ModelSerializer):
    debate_interactions = DebateInteractionSerializer(many=True, read_only=True)
    category_detail = serializers.StringRelatedField(read_only=True, source='category')


    class Meta:
        model = DebateSession
        fields = (
            'id',
            'url',
            'name',
            'user',
            'category',
            'category_detail',
            'session_max_duration',
            'response_max_duration',
            'debate_interactions',
            'created_at',
            'finished_at',
        )


class DeboteUserSerializer(serializers.ModelSerializer):
    debate_sessions = DebateSessionSerializer(many=True, required=False)

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'is_superuser',
            'email',
            'groups',
            'debate_sessions',
            'password'
        )
        read_only_field = ('id', )
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )

        user.set_password(validated_data['password'])
        user.save()

        return user


class GroupSerializer(serializers.ModelSerializer):
    users = DeboteUserSerializer(many=True, read_only=False)

    class Meta:
        model = Group
        fields = (
            'id',
            'name',
            'users',
        )


class MyUserGroupSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=True, required=False)

    class Meta:
        model = User
        fields = (
            'username',
            'is_superuser',
            'first_name',
            'last_name',
            'email',
            'groups',
            'password'
        )
        read_only_field: ('id', )