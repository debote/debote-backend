from django.contrib.auth.models import User, Group
from django.forms import model_to_dict
from django.http import Http404
from django.middleware.csrf import get_token
from rest_framework import viewsets, permissions, generics, renderers, status
from rest_framework.decorators import api_view, action
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.settings import api_settings

from debote.models import Category, Question, DebateSession, DebateInteraction
from debote.serializers import DeboteUserSerializer, GroupSerializer, CategorySerializer, QuestionSerializer, \
    DebateSessionSerializer, DebateInteractionSerializer, MyUserGroupSerializer, CategoryOnlySerializer

# Create your views here.
from ml import MachineLearning


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = DeboteUserSerializer
    # permission_classes = [permissions.IsAuthenticated]


class MyUserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = MyUserGroupSerializer
    # permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [permissions.IsAuthenticated]


class CategoryOnlyViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategoryOnlySerializer
    permission_classes = [permissions.IsAuthenticated]


class DebateSessionViewSet(viewsets.ModelViewSet):
    queryset = DebateSession.objects.all()
    serializer_class = DebateSessionSerializer
    permission_classes = [permissions.IsAuthenticated]


class DebateInteractionViewSet(viewsets.ModelViewSet):
    queryset = DebateInteraction.objects.all()
    serializer_class = DebateInteractionSerializer

    permission_classes = [permissions.IsAuthenticated]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        if request.data.get('response_by') == 'usr':
            category = Category.objects.get(pk=request.data.get('category'))
            category = model_to_dict(category)

            question = Question.objects.get(pk=request.data.get('question'))
            question = model_to_dict(question)

            list_of_question = []
            for value in Question.objects.filter(category_id=request.data.get('category')).values_list('id',
                                                                                                       'question'):
                list_of_question.append({
                    'id': value[0],
                    'question': value[1],
                })

            result = MachineLearning.find_similar(question.get('question'), category.get('category'), list_of_question)

            headers = self.get_success_headers(serializer.data)
            return Response({
                'result': result,
                'usrResponse': serializer.data,
            }, status=status.HTTP_201_CREATED, headers=headers)
        else:
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save()

    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}


class QuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    permission_classes = [permissions.IsAuthenticated]


@api_view(['GET'])
def get_csrf_token(request):
    token = get_token(request)
    return Response({'token': token})


@api_view(['GET'])
def get_count(request):
    return Response({
        'sessions': DebateSession.objects.count(),
        'categories': Category.objects.count(),
        'questions': Question.objects.count(),
        'users': User.objects.count(),
    })


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'questions': reverse('question-list', request=request, format=format),
        'categories': reverse('category-list', request=request, format=format)
    })


@api_view(['POST'])
def train_data(request):
    if request.data.get('category_id'):
        catch_target_category = request.data.get('category_id')
        try:
            category = Category.objects.get(pk=catch_target_category)
            list_of_question = []
            for value in Question.objects.filter(category_id=catch_target_category).values('question'):
                list_of_question.append(value.get('question'))
            serialize = model_to_dict(category)

            MachineLearning.train_dataset(list_of_question, serialize.get('category'))
            return Response({
                'message': 'Success'
            })
        except Category.DoesNotExist:
            raise Http404
    else:
        return Response({'message': 'No category id in request body'}, status=status.HTTP_400_BAD_REQUEST)
