from django.urls import path, include
from rest_framework import routers, renderers
from rest_framework.urlpatterns import format_suffix_patterns
from debote import views

router = routers.DefaultRouter()
router.register(r'categories', views.CategoryViewSet)
router.register(r'categoryOnly', views.CategoryOnlyViewSet)
router.register(r'questions', views.QuestionViewSet)
router.register(r'sessions', views.DebateSessionViewSet)
router.register(r'interactions', views.DebateInteractionViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('train/', views.train_data),
    path('count/', views.get_count)
]

# urlpatterns = format_suffix_patterns(urlpatterns)
