from django.contrib import admin
from debote.models import Category, Question, DebateSession, DebateInteraction

# Register your models here.
admin.site.register(Category)
admin.site.register(Question)
admin.site.register(DebateSession)
admin.site.register(DebateInteraction)
