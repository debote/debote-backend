from datetime import datetime

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _


# Create your models here.
class Category(models.Model):
    class Meta:
        verbose_name_plural = 'categories'

    objects = models.Manager()

    category = models.CharField(max_length=255)
    language = models.CharField(max_length=2)
    created_at = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField()

    def __str__(self):
        return self.category

    def get_absolute_url(self):
        return f'/{self.slug}/'


class Question(models.Model):
    objects = models.Manager()

    question = models.CharField(max_length=500)
    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE,
        related_name="questions"
    )
    slug = models.SlugField()

    def __str__(self):
        return self.question

    def get_absolute_url(self):
        return f'/{self.category}/{self.slug}/'


class DebateSession(models.Model):
    objects = models.Manager()

    class Meta:
        db_table = 'debote_debate_sessions'

    name = models.CharField(
        max_length=500,
        default=datetime.now()
    )
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="debate_sessions"
    )
    category = models.ForeignKey(
        Category,
        on_delete=models.SET_NULL,
        null=True,
        related_name="debate_sessions"
    )
    session_max_duration = models.IntegerField(default=0, null=True, blank=True)
    response_max_duration = models.IntegerField(default=0, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    finished_at = models.DateTimeField(null=True)

    def __str__(self):
        return self.name


class DebateInteraction(models.Model):
    objects = models.Manager()

    class Meta:
        db_table = 'debote_debate_interactions'

    class ResponseChoice(models.TextChoices):
        USER = 'usr', _('User')
        SYSTEM = 'sys', _('System')

    debate_session = models.ForeignKey(
        DebateSession,
        on_delete=models.CASCADE,
        related_name="debate_interactions"
    )
    question = models.ForeignKey(
        Question,
        null=True,
        on_delete=models.SET_NULL,
        related_name="debate_interactions"
    )
    confidence = models.FloatField(null=True)
    response_by = models.CharField(
        max_length=3,
        choices=ResponseChoice.choices,
        default=ResponseChoice.USER
    )
    answered_at = models.DateTimeField(auto_now_add=True)
